package com.example.listadetareas;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {



    ArrayList<String> titulo, desc, fecha, grado, total;
    EditText eTitulo, eDesc, eFecha;
    RadioButton rbGradoA, rbGradoM, rbGradoB;
    Button btnGuardar;
    RadioGroup gr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        titulo  = new ArrayList<String>();
        desc  = new ArrayList<String>();
        fecha = new ArrayList<String>();
        grado  = new ArrayList<String>();
        total  = new ArrayList<String>();

        eTitulo = (EditText) findViewById(R.id.editTextTitulo);
        eDesc = (EditText) findViewById(R.id.editTextDesc);
        eFecha = (EditText) findViewById(R.id.editTextFechaR);
        gr = (RadioGroup) findViewById(R.id.groupRadio);
        rbGradoA = (RadioButton) findViewById(R.id.radioButtonGA);
        rbGradoM = (RadioButton) findViewById(R.id.radioButtonGM);
        rbGradoB = (RadioButton) findViewById(R.id.radioButtonGB);
        btnGuardar = (Button) findViewById(R.id.buttonGuardar);
        Button bMost = (Button) findViewById(R.id.buttonMost);

        bMost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), mostrar_tareas.class);
                intent.putExtra("Titulos", titulo);
                startActivity(intent);
            }
        });

        eFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog picker;
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(MainActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                eFecha.setText(dayOfMonth + "/" + (monthOfYear+1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if((eTitulo.getText().toString().isEmpty()) || (eTitulo.getText().toString().isEmpty()) || (eFecha.getText().toString().isEmpty()) || ((rbGradoA.isChecked() == false)&&(rbGradoB.isChecked() == false) && (rbGradoM.isChecked()==false))) {
                    Toast.makeText(getApplicationContext(), "Completar todos los datos", Toast.LENGTH_SHORT).show();
                }
                else{
                    guardarDatos();
                    enviarDatos();
                    eTitulo.setText("");
                    eFecha.setText("");
                    eDesc.setText("");
                    gr.clearCheck();

                }

            }
        });
    }

    public void guardarDatos(){
        String tit, descp,  fechaR, gradoI=null;

        tit = eTitulo.getText().toString();
        descp = eDesc.getText().toString();
        fechaR = eFecha.getText().toString();
        if(rbGradoA.isChecked() == true){
            gradoI = rbGradoA.getText().toString();

        }
        if(rbGradoM.isChecked() == true){
            gradoI = rbGradoM.getText().toString();
        }
        if(rbGradoB.isChecked() == true){
            gradoI = rbGradoB.getText().toString();

        }
        titulo.add("PRIORIDAD " + gradoI + "\n" + tit + "\n" + descp + "\n" + fechaR);

    }

    public void enviarDatos(){
        Intent i = new Intent(this, mostrar_tareas.class);
        i.putExtra("Titulos", titulo);
        startActivity(i);
    }

}