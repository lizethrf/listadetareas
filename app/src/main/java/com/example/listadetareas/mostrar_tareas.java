package com.example.listadetareas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

public class mostrar_tareas extends AppCompatActivity {

    ListView listaTareas;
    ArrayAdapter<String> elementos;
    ArrayList<String> tituloTareas;
    ArrayList<String> descTareas;
    ArrayList<String> fecha;
    ArrayList<String> grado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_tareas);

        listaTareas = (ListView) findViewById(R.id.ListViewTareas);

        tituloTareas = new ArrayList<String>();
        descTareas = new ArrayList<String>();
        fecha = new ArrayList<String>();
        grado = new ArrayList<String>();


        //Obtener el intent del activity
        tituloTareas = (ArrayList<String>) getIntent().getSerializableExtra("Titulos");

        //Mandar elementos al ListView
        elementos = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tituloTareas);

        listaTareas.setAdapter(elementos);
    }
}